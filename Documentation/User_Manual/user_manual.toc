\contentsline {chapter}{\numberline {1}Copyright}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Copyright / Copyleft}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}Zealeov}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Synopsis}{3}{section.2.1}
\contentsline {paragraph}{presentation}{3}{section*.2}
\contentsline {section}{\numberline {2.2}Interface}{3}{section.2.2}
\contentsline {paragraph}{description}{3}{section*.3}
\contentsline {subsection}{\numberline {2.2.1}Syntaxs}{3}{subsection.2.2.1}
\contentsline {paragraph}{How to use Zealeov}{3}{section*.4}
\contentsline {paragraph}{Use}{3}{section*.5}
\contentsline {paragraph}{parameters}{4}{section*.6}
\contentsline {subsection}{\numberline {2.2.2}Options}{4}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}DataBase}{4}{section.2.3}
\contentsline {paragraph}{Connect to database}{4}{section*.7}
\contentsline {section}{\numberline {2.4}Logging}{4}{section.2.4}
\contentsline {paragraph}{description}{4}{section*.8}
\contentsline {chapter}{\numberline {3}Behaviors}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Specificity}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}Requirement}{5}{section.3.2}
\contentsline {section}{\numberline {3.3}Version}{5}{section.3.3}
\contentsline {section}{\numberline {3.4}File}{6}{section.3.4}
\contentsline {paragraph}{file repository}{6}{section*.9}
\contentsline {section}{\numberline {3.5}Documentation}{6}{section.3.5}
\contentsline {section}{\numberline {3.6}Licence and Copyright}{6}{section.3.6}
