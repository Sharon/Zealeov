
"""
    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

"""
from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from .Journal import Journal
from .Metier import Playlist


class Output():

    def __init__(self, playlist):
        self.playlist = playlist
        self.journal = Journal()
        
    def write_m3u(self, display_result):
        """Write parts to a M3U file
    
        Keyword Arguments:
        filename -- file name
        the_playlist -- list of paths to write in the file
        """
        if len(self.playlist) > 0:
            try:
                extension = '.M3U'
                self.playlist.nom += extension
                path_to_file = "playlists/%s" % self.playlist.nom
                my_file = open(path_to_file, "w")
                for song in self.playlist:
                    my_file.write("{} \n".format(str(song)))
                    if display_result:
                         print(song)
                my_file.close()
                error_message = 'The playlist %s is save ' % path_to_file
                error_message += 'and contains %s parts' % len(self.playlist)
                self.journal.journal.info(str(error_message))
            except IOError:
                error_message = "The file %s has occured an error" % self.playlist.nom
                self.journal.journal.error(str(error_message))
                verbosity_error_message = "file not found: {}".format(self.playlist.nom)
                self.journal.journal.critical(str(verbosity_error_message))
    
    def write_xspf(self):
        """Write elements to a XSPF file
    
        Keyword Arguments:
        filename -- file name
        the_playlist -- list of paths to write in the file
        """
        #<?xml version="1.0" encoding="UTF-8"?>
        try:
            extension = '.xspf'
            self.playlist.nom += extension
            path_to_file = "playlists/%s" %  self.playlist.nom
            playlist = Element("playlist")
            playlist.set("version", "1")
            playlist.set("xmlns", "http://xspf.org/ns/0/")
            track_list = SubElement(playlist, "trackList")
            for song in self.playlist:
                track = SubElement(track_list, "track")
                SubElement(track, "location").text = str(song)
            tree = ElementTree(playlist)
            tree.write(path_to_file, xml_declaration="""<?xml version="1.0" encoding="UTF-8"?>""")
            error_message = 'The playlist %s is save ' % path_to_file
            error_message += 'and contains %s parts' % len(self.playlist)
            self.journal.journal.info(str(error_message))
        except IOError:
            error_message = "The file %s has occured an error" % self.playlist.nom
            self.journal.journal.error(str(error_message))
            verbosity_error_message = "file not found: {}".format(self.playlist.nom)
            self.journal.journal.critical(str(verbosity_error_message))
