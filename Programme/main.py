#!/usr/bin/python3
"""
Created on Wed Sep 20 09:53:06 2017


    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Desire
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.


"""
from .CLI import CLI
from .Helpers import Launch_generator
from .Journal import Journal
from .Configuration import Setup_configuration
from .Configuration import Show_configuration

journal = Journal()
# more or less. verbose display depending on the parameter -v

def start():

    journal.configuration_sortie_console()
    interface = CLI()
    args = interface.Options()
    journal.configuration_sortie_dans_fichier(args.log, "zealeov.log")
    journal.Set_log_level_from_verbose(args)
    
    if args.show:
        Show_configuration()
       
    if args.config:
       if Setup_configuration():
           journal.journal.info("Launch the generator")
           Launch_generator(args)       
    else:
        Launch_generator(args)
 
    
