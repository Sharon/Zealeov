#!/usr/bin/python3
"""
Created on Wed Sep 20 09:53:06 2017


    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Desire
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.


"""
import logging
from logging.handlers import TimedRotatingFileHandler
import os

class Journal():
    def __init__(self, chemin='/tmp/Zealeov', filename='zealeov.log'):
        # We configure the logs for the generator
        self.level_console = logging.ERROR
        self.level_fichier = logging.DEBUG
        self.chemin = chemin
        self.filename = filename
        self.journal = logging.getLogger()
        self.journal.setLevel(logging.DEBUG)
        self.formatter = '%(asctime)s [%(levelname)s](%(funcName)s:%(lineno)d): %(message)s'

    def configuration_sortie_dans_fichier(self, chemin, filename):
        # Redirect all logs in the file name 'Zealeov.log'
        if not os.path.exists(os.path.join(chemin, filename)):
            if not os.path.exists(chemin):
                os.mkdir(chemin)
            elif not os.path.exists(filename):
                os.system("touch {}".format(filename))
                
        log_file_handler = TimedRotatingFileHandler(os.path.join(chemin, filename), when='M', interval=2)
        log_file_handler.setFormatter(logging.Formatter(self.formatter))
        log_file_handler.setLevel(self.level_fichier)
        self.journal.addHandler(log_file_handler)

    def configuration_sortie_console(self):
        # Redirect all logs to the console
        console_handler = logging.StreamHandler() # sys.stderr
        console_handler.setLevel(self.level_console)
        console_handler.setFormatter(logging.Formatter(self.formatter))
        self.journal.addHandler(console_handler)
        
     
    def Set_log_level_from_verbose(self,cli_args):
        """Set log l.evel from verbose or
        if the user d.ecide to switch in debug mode.
    
        Keyword Arguments:
        cli_args -- user input
        """
        if not cli_args.verbose:
            self.level_console = logging.CRITICAL
        elif cli_args.verbose == 1:
            self.level_console = logging.ERROR
        elif cli_args.verbose == 2:
            self.level_console = logging.INFO
        elif cli_args.verbose >= 3:
            self.level_console = logging.DEBUG
     
        if cli_args.debug:
            self.level_console = logging.DEBUG 
        
        self.configuration_sortie_console()
        
journal = Journal()
