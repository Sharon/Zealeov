#!/usr/bin/python3
"""
Created on Wed Sep 20 09:53:06 2017


    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Desire
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.


"""
# Connect to database radio-libre with Postgresql
import psycopg2
from .Configuration import Run_configuration
from .Journal import Journal
from .Metier import Morceau
from .Metier import Playlist


class Connection():
    
    def __init__(self):
        self.journal = Journal()
    
    def Connect(self):
        
        self.connection_path = Run_configuration()
        self.connexion = psycopg2.connect(self.connection_path)
        self.cursor = self.connexion.cursor()
        self.journal.journal.info('Connect to database sucess')
    
    def Execute(self, request):
        """Manage the connection to the database
        Keyword Arguments:
        request -- request recovered in the where clause generator
        """
        playlist = Playlist()
        try:
            if request != None:
                self.cursor.execute(request)
                rows = self.cursor.fetchall()
                
                    
                for part in rows:
                    morceau = Morceau(part[0],part[1], part[2])
                    playlist.inserer(morceau)
    
                if len(rows) > 0:
                    self.journal.journal.info('%s parts were recovered' % len(rows))
                else:
                    self.journal.journal.error('No parts was recovered')
            return playlist
        except psycopg2.DatabaseError as error:
            self.journal.journal.critical(error)
                
            
    def Close(self):
        self.cursor.close()
        self.connexion.close()
