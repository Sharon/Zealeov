#!/usr/bin/python3

# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 09:53:06 2017


    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Désiré
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

"""
import random
from .Journal import Journal

class Playlist(list):
    """
    Représente une playlist faisant référence à plusieurs Morceaux
    """
    def __init__(self, nom="default-playlist", duree=3600, marge=30):
        self.nom = nom
        self.duree_max = duree
        self.marge = marge
        self.journal = Journal()
        
    def inserer(self, morceau):
        self.append(morceau)
        
    def duree(self):
        somme = 0
        for morceau in self:
            somme += morceau.duree
        return somme
        

        
    def creer(self, the_playlist, arguments, total_duration, margin):
        """Playlists Management :
        Function that manage the playlists on criterias of duration

        Keyword Arguments:
        the_playlist -- the list of parts
        argument --  type of seach in the playlist
        critere -- name of attribut of playlist
        """
        final_duration = 0 
        if len(the_playlist) > 0:
            try:
                self.duree_max += int(arguments) / 100 * total_duration 
                console_info = "The total of playlist wanted is %s seconds." % self.duree_max
                self.journal.journal.info(str(console_info))
                duree_morceau = 0
                for morceau in the_playlist:
                    duree_morceau += int(morceau.duree)
                    if duree_morceau <= (self.duree_max-margin):
                        self.append(str(morceau.chemin))
                        final_duration = (self.duree_max-margin) - duree_morceau
            except TypeError as error:
                self.journal.journal.critical("ERROR : %s " % error)
                self.journal.journal.info("Required an integer")
                exit()
            random.shuffle(self)
            console_debug = "The margin of the playlist is %s seconds and contains %s parts." % (final_duration,len(self))    
            print(console_debug)
            self.journal.journal.debug(str(console_debug))
        else:
            self.journal.journal.critical("The playlist is empty")
        return self

    def check_margin(self, margin, total_duration):
        isRight = False
        if margin <=  total_duration * 0.01:
            self.journal.journal.info("The margin is correct")
            self.journal.journal.debug("The choosen margin %s is inferior to the maximum margin : %s." % (margin ,  total_duration * 0.01))
            isRight = True
        else:
            self.journal.journal.error("Wrong margin")
            self.journal.journal.critical("The choosen margin %s is superior to the maximum margin : %s." % (margin, total_duration* 0.01))
            isRight = False
        return isRight
        
    def __str__(self):
        playlist = ""
        for morceau in self:
            playlist += str(morceau)
        return playlist
        
class Morceau():
    """
    Représente un morceau tel que disponible dans la base de données
    """
    def __init__(self, genre, duree, chemin):
        self.genre = genre
        self.duree = duree
        self.chemin = chemin
        
    def __str__(self):
        return chemin
