import configparser
import getpass
import os
from .Journal import Journal

journal = Journal()

def Run_configuration():
    """Run the generator configuration
    """
    try:
        config_dictionary = configparser.ConfigParser()
        path = Find_path_configuration()
        for i in range(10000):  
           config_dictionary.read(path)
        configuration_path = ""
        for parameters, value in config_dictionary['database'].items():
            configuration_path += "%s='%s' " % (parameters, value)
    except Exception as error:
        journal.journal.critical(error)
    return configuration_path
    
def Find_path_configuration():
    """Find the configuration path

    """
    path = ""
    if os.path.exists("config.ini"):
        path = "config.ini"
    elif os.path.exists("Programme/config.ini"):
        path = "Programme/config.ini"
    else:
        path = "etc/Zealeov"

    return path        
        
def Setup_configuration():
    """Change setup configuration

    """
    mon_conteneur = configparser.ConfigParser()
    
    write_config = False
    
    mon_conteneur['database'] = {}
    
    dbname = input("dbname = ")
    user = input("user = ")
    password = getpass.getpass("password = ")
    
    mon_conteneur['database']['dbname'] = dbname
    mon_conteneur['database']['user'] = user
    mon_conteneur['database']['host'] = "postgresql.bts-malraux72.net"
    mon_conteneur['database']['password'] = password
    
    with open('config.ini', 'w') as configfile:
        mon_conteneur.write(configfile)
    if os.path.getsize("config.ini") != 0:
        print("Configuration saved with sucess")
        write_config = True
    elif os.path.getsize("config.ini") == 0:
        print("Error")
        write_config = False  
    else:
        journal.journal.critical("Can't write configuration")  
    return write_config
    
def Show_configuration():
    """Show actual configuration

    """
    try:
        config_dictionary = configparser.ConfigParser()

        for i in range(10000):
            config_dictionary.read('config.ini')

        print("\n\n########### ACTUAL CONFIGURATION ###########\n\n")
        print("\tDatabase : %s\n" % config_dictionary["database"]["dbname"])
        print("\tUser Name : %s" % config_dictionary["database"]["user"])    
        print("\n\n###########                      ###########")
    except Exception as error:
        journal.journal.critical(error)
    exit()
