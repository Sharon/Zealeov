#!/usr/bin/python3
"""
Created on Wed Sep 20 09:53:06 2017


    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Desire
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.


"""
#
#  ██████╗ ████████╗███████╗    ███████╗██╗ ██████╗ 
#  ██╔══██╗╚══██╔══╝██╔════╝    ██╔════╝██║██╔═══██╗
#  ██████╔╝   ██║   ███████╗    ███████╗██║██║   ██║
#  ██╔══██╗   ██║   ╚════██║    ╚════██║██║██║   ██║
#  ██████╔╝   ██║   ███████║    ███████║██║╚██████╔╝
#  ╚═════╝    ╚═╝   ╚══════╝    ╚══════╝╚═╝ ╚═════╝ 
#                                                   

import argparse

class CLI():
    def __init__(self):
        self.cli = argparse.ArgumentParser("Playlist Generator : Zealeov")


    def Options(self):
        self.cli.add_argument(
            '-V', '--version',
            action="version",
            version="%(prog)s 1.0.1")
        self.cli.add_argument(
            '--file',
            help="name of playlist",
            default = "default_playlist" )
        self.cli.add_argument(
            '--duration',
            help="duration of playlist",
             metavar='DUREE',
            type=int,
            default = 3600)
        self.cli.add_argument(
            '-v', '--verbose',
            help='Increase the language verbosity',
            action="count",
            default=0)
        self.cli.add_argument(
            '-d', '--debug',
            help='switch to debug mode',
            action='store_true',
            default=0)
        self.cli.add_argument(
            '--genre',
            help='retrieves the songs by genre passed in arguments',
            nargs=2,
            action='append')
        self.cli.add_argument(
            '--artist',
            help='retrieves the songs by artist passed in arguments',
            nargs=2,
            action='append')
        self.cli.add_argument(
            '--album',
            help='retrieves the songs by album passed in arguments',
            nargs=2,
            action='append')
        self.cli.add_argument(
            '--title',
            help='retrieves the songs by title passed in arguments',
            nargs=2,
            metavar=('TITLE', int),
            action='append')
        self.cli.add_argument(
            '--output',
            help='output type of the playlist',
            type=str)
        self.cli.add_argument(
            '--config',
            action='store_true',
            help='setup the database configuration'
            )
        self.cli.add_argument(
            '--margin',
            type=int,
            default=0,
            help='configure the margin of the playlist'
            )
        self.cli.add_argument(
            '--show',
            action="store_true",
            help="show the actual database access configuration"
            )
        self.cli.add_argument(
            '--log',
            action="store",
            type=str,
            default="/tmp/Zealeov",
            help="Change the default log repository destination"
            )   
        args = self.cli.parse_args()
        return args