#!/usr/bin/python3
"""
Created on Wed Sep 20 09:53:06 2017


    Zealeov Copyright (C) 2017  Panthier Sharon-Joyce & Patureau Desire
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.


"""
import os
from .Journal import Journal
from .Metier import Playlist
from .Connexion import Connection
from .Sortie import Output


journal = Journal()


def Launch_generator(cli_args):
    """Manage all arguments user's input

    Keyword Arguments:
    cli_args -- user input
    """
    #try:
    output_playlist = Playlist(cli_args.file, cli_args.duration, cli_args.margin)
    argument = ""
    critical_error = ""
    sigle = [['^$', 0]]
        
    if output_playlist.check_margin(cli_args.margin, cli_args.duration):
        for critere in ['genre', 'album', 'title', 'artist']:
            argument = getattr(cli_args, critere, None)
                 
            if argument is None:
               argument = sigle
            if critere == "title":
               critere = "titre"
            if critere == "artist":
               critere = "artiste"     

            if(calculate_pourcent(cli_args)):
                new_playlist = GeneratorSQL(argument, critere, cli_args.duration, cli_args.margin)
                if len(new_playlist) != 0:
                    output_playlist = new_playlist
            else:
                critical_error = "The choosen argurment must be equals to 100"
                journal.journal.critical(str(critical_error))
                exit()
           
        Write_playlsts(cli_args, output_playlist)
        
    #except Exception as error:
    #    journal.journal.critical(error)
        
def Write_playlsts(cli_args, playlist):
    if not os.path.exists('playlists'):
        os.mkdir('playlists')
    output = Output(playlist)
    
    playlist.nom = cli_args.file
    if not cli_args.output:
       output.write_m3u(True)
    elif cli_args:
       output_type = cli_args.output.upper()
       if output_type == "M3U":
           output.write_m3u(False)
       elif output_type == "XSPF":
           output.write_xspf()
       else:
           journal.journal.error("There is an error of extension")
               
def calculate_pourcent(cli_args):
    pourcent_duration = 0
    isValide = True
    for critere in ['genre', 'album', 'title', 'artist']:
        argument = getattr(cli_args, critere, None)
        if not argument == None:
           for pourcentage in argument:             
               pourcent_duration += int(pourcentage[1])    
               if pourcent_duration == 100:
                   isValide = True
               else:
                   isValide = False    
    return isValide
    
def expression(word):
    """Convert the string with the ASCII Table

    Keyword Arguments:
    word -- word to convert
    """
    if word != '^$' and word != '^ $':
        sub_string = word[0]
        change_to_ascii = ord(sub_string)
        rest = ''
        letter = ""
        for i in range(1, len(word)):
            rest += word[i].lower()
        if change_to_ascii >= 97 and change_to_ascii <= 123:
            letter = "[%s%s]%s" % (chr(change_to_ascii-32), sub_string, rest)
        elif change_to_ascii >= 65 and change_to_ascii <= 90:
            letter = "[%s%s]%s" % (sub_string, chr(change_to_ascii+32), rest)
        else:
            letter = word
        return letter
    else:
        letter = word
        return letter
        
def GeneratorSQL(arguments, criteria, total_duration, margin = 30):
    """generator of where clause in sql

    Keyword Arguments:
    arguments -- name of argument to search in database
    criteria -- criteria of attribute
    add_request -- end of sql request can be change
    """
    sort_playlist = Playlist()
    if not arguments[0][0] == '^$':
        start_sql_request = "select genre, duree, chemin from \"examen.projet1\".morceaux where "
        where = ""
        playlist_unsorted = Playlist()
        sql = ""
        for element in arguments:
            where = ""
            sql = ""
            search = expression(element[0])
            where = "%s ~ '%s*'" % (criteria, search)
            sql = start_sql_request + where + " order by random();"
            journal.journal.debug(sql)
            
            # Connect to the database
            connect = Connection() 
            connect.Connect()
            playlist_unsorted = connect.Execute(sql)
            connect.Close()    
            
            if playlist_unsorted != None:
                sort_playlist.creer(playlist_unsorted, element[1], total_duration, margin)
            else:
                info_messages = "There is no playlists"
                journal.journal.info(str(info_messages))
    else:
        journal.journal.info('No arguments for this section')
    return sort_playlist
    

